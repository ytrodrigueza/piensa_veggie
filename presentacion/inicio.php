<?php
?>



<div class="container-fluid bg-dark">
<div class="row">
<div class="col-lg-3 col-md-4 text-center" >
	<img id="imagen" src="img/Logo.png" alt="logo principal" width="300"/>
</div>
<div class="col-lg-9 col-md-12">
	<h1 class="text-center" id="titulo" >
		<i><huge>Piensa Veggie</huge></i>
	</h1>
		
</div>
</div>
</div>

<nav class="navbar navbar-expand-lg  bg-dark">
  <div class="container-fluid">
     <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><img src="img/logo_pesta.png" alt="logo principal" width="50"/></a>
    <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
      <ul class="navbar-nav">
      <li class="nav-item" id="productos"><a class="nav-link"
				href="index.php?pid=<?php echo base64_encode("presentacion/articulos.php")?>">Productos</a></li>
			<li class="nav-item"><a class="nav-link"
				href="index.php?pid=<?php echo base64_encode("presentacion/registro.php")?>">Registrarse</a></li>
         
      </ul>
       
    </div>
  </div>

</nav>

<!-- Carrusel de bienvenidos publicidad -->
<div id="carouselExampleFade" class="carousel carousel-dark slide carousel-fade" data-bs-ride="carousel">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleFade" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
 
  <div class="carousel-inner">
    <div class="carousel-item active">
    <a href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><img src="img/bienvenido.jpg" class="d-block w-100" alt="bienvenido"  height="50%"></a>
     <div class="carousel-caption d-none d-md-block">
        <h5>Bienvenido</h5>
        <p>Aca encontrará variedad de productos de origen no animal</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/violeta.jpg" class="d-block w-100" alt="libre de crueldad" height="50%">
     <div class="carousel-caption d-none d-md-block">
        <h5>Por una alimentación diferente</h5>
        <p>"Que la nobleza de tu corazón supere la gula de tu paladar"</p>
      </div>
    </div>
    <div class="carousel-item">
     <a href="index.php?pid=<?php echo base64_encode("presentacion/registro.php")?>"> <img src="img/registro.jpg" class="d-block w-100" alt="registro"  height="50%"></a>
   <div class="carousel-caption d-none d-md-block">
        <h5>¿Deseas adquirir alguno de nuestros productos?</h5>
        <p>Necesitas registrarte, haz clic aca.</p>
      </div>
   </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Anterior</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Siguiente</span>
  </button>
</div>







<?php 
/*
<div id="resultados"> Resultados gráficos</div>



<script>
$(document).ready(function() {
 
$("#productos").click(function(){
var url="?pid=<?php echo base64_encode("presentacion/articulos.php")?>"
$("#resultados").load(url);
alert("Hizo click en productos");

});
});

</script>
*/
?>

