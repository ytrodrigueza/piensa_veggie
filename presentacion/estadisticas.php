<?php
include "presentacion/inicio1.php";
$articulo = new Articulo();
$articulosPorMarca = $articulo ->consultarProductosPorMarca();
$articulosPorCategoria = $articulo -> consultarProductosPorCategoria();

?>

<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Estadisticas</h5>
				<div class="card-body ">
					<div id="productosPorMarca" style="width: 1000px; height: 300px;"></div>
					<div id="productosPorCategoria" style="width: 1000px; height: 300px;"></div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    var dataMarca = google.visualization.arrayToDataTable([
        ["Marca", "Cantidad"],
        <?php 
        foreach ($articulosPorMarca as $a){
            echo "['" . $a[0] . "', " . $a[1] . "],\n";        
        }        
        ?>
    ]);
    
    var viewMarca = new google.visualization.DataView(dataMarca);
    
    var optionsMarca = {
        title: "Articulos por Marca",
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
    };
    var chartMarca = new google.visualization.ColumnChart(document.getElementById("articulosPorMarca"));
    chartMarca.draw(viewMarca, optionsMarca);

//fin de primer grafico
    
    var dataTipo = google.visualization.arrayToDataTable([
        ["Categoria", "Cantidad"],
        <?php 
        foreach ($articulosPorCategoria as $a){
            echo "['" . $a[0] . "', " . $a[1] . "],\n";        
        }        
        ?>
    ]);
    
    var viewTipo = new google.visualization.DataView(dataTipo);
    
    var optionsTipo = {
        title: "Articulos por categoria",
        bar: {groupWidth: "95%"},
        legend: { position: "right" },
        is3D: true,
    };
    var chartTipo = new google.visualization.PieChart(document.getElementById("articulosPorCategoria"));
    chartTipo.draw(viewTipo, optionsTipo);





}
</script>