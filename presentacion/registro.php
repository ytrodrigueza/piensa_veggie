<?php

include "presentacion/inicio1.php";
require_once 'logica/Cliente.php';
require_once 'logica/Administrador.php';
require_once 'logica/Domiciliario.php';

if(isset($_POST["registro"])){
   
    $cliente=new Cliente("",$_POST["nombre"], $_POST["apellido"], $_POST["correo"], $_POST["clave"],2);
    $cliente->registrar();

 /*  if(isset($_POST["rol"])==3) {
        $administrador=new Administrador("", $_POST["nombre"], $_POST["apellido"], $_POST["correo"], $_POST["clave"],3);   
        $administrador->registrar();
    }else if (isset($_POST["rol"])==2){
        $cliente=new Cliente("",$_POST["nombre"], $_POST["apellido"], $_POST["correo"], $_POST["clave"],2);
        $cliente->registrar();
     
    }else {
        $domiciliario=new Domiciliario("",$_POST["nombre"], $_POST["apellido"], $_POST["correo"], $_POST["clave"],1);
        $domiciliario->registrar();
   
    }
    */

    
    
}

?>

<h1>Registro</h1>

				
					
<div class="container">
	<div class="row mt-3">
		<div class="col-4 col-sm-3 col-lg-2 text-center">
			<img src="img/logo_pesta.png" width="60%">

		</div>
		<div class="col-8 col-sm-9 col-lg-10">
			<h1 class="text-center">Piensa Veggie</h1>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-sm-0 col-md-3"></div>
		<div class="col-sm-12 col-md-6">
			<div class="card">
				<h5 class="card-header">Registrarse</h5>
				<div class="card-body">
					<?php if (isset($_POST["registro"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
						role="alert">
						Datos ingresados correctamente.
						<button type="button" class="btn-close" data-bs-dismiss="alert"
							aria-label="Close"></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/registro.php") ?>" method="post">
							<div class="mb-3">
							<label class="form-label">Nombre</label>
							<input type="text" class="form-control" name="nombre" required="required">
						</div>
						<div class="mb-3">
							<label class="form-label">Apellido</label>
							<input type="text" class="form-control" name="apellido" required="required">
						</div>
						<div class="mb-3">
							<label class="form-label">Correo</label>
							<input type="email" class="form-control" name="correo" required="required">
						</div>
						<div class="mb-3">
							<label class="form-label">Clave</label>
							<input type="password" class="form-control" name="clave" required="required">
						</div>
					<?php /* <div class="mb-3">
							<label class="form-label">Rol</label>
							<select class="form-select" name="rol">
							<option value="-1">Seleccione Rol</option>
					<option value="1">Domiciliario</option>
					<option value="2">Cliente</option>
					<option value="3">Administrador</option>						
							</select>
						</div>   */?>	
						
						
						<div class="d-grid">
							<button type="submit" name="registro" class="btn btn-primary">Registrarse</button>
						</div>
						<div class="d-grid">
						<a href="index.php"><button type="button" class="btn btn-outline-secondary" >Regresar</button></a>
							
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>