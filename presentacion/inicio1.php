<?php

?>

<div class="container-fluid bg-dark">
<div class="row">
<div class="col-lg-3 col-md-4 text-center" >
	<img id="imagen" src="img/Logo.png" alt="logo principal" width="300"/>
</div>
<div class="col-lg-9 col-md-12">
	<h1 class="text-center" id="titulo" >
		<i><huge>Piensa Veggie</huge></i>
	</h1>
		
</div>
</div>
</div>

<nav class="navbar navbar-expand-lg  bg-dark">
  <div class="container-fluid">
     <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/inicio.php")?>"><img src="img/logo_pesta.png" alt="logo principal" width="50"/></a>
    <button class="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
      <ul class="navbar-nav">
      <li class="nav-item" id="productos"><a class="nav-link"
				href="index.php?pid=<?php echo base64_encode("presentacion/articulos.php")?>">Productos</a></li>
			<li class="nav-item"><a class="nav-link"
				href="index.php?pid=<?php echo base64_encode("presentacion/registro.php")?>">Registrarse</a></li>
			<li class="nav-item"><a class="nav-link"
				href="index.php?pid=<?php echo base64_encode("presentacion/estadisticas.php")?>">Estadísticas</a></li>
         
      </ul>
       
    </div>
  </div>

</nav>