<?php

require_once "persistencia/Conexion.php";
require_once "persistencia/ArticuloDAO.php";

class Articulo{
    private $id;
    private $nombre;
    private $stock;
    private $precio;
    private $imagen;
    private $marca;
    private $categoria;
    private $administrador;
    private $conexion;
    private $articuloDAO;
    
       
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    
    public function getPrecio()
    {
        return $this->precio;
    }

   
    public function getImagen()
    {
        return $this->imagen;
    }

    
    public function getMarca()
    {
        return $this->marca;
    }

   
    public function getCategoria()
    {
        return $this->categoria;
    }

   
    public function getAdministrador()
    {
        return $this->administrador;
    }

   

    public function Articulo($id="", $nombre="", $stock="", $precio="",  $imagen="",$marca="",$categoria="", $administrador="" ){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> stock = $stock;
        $this -> precio = $precio;
        $this -> imagen = $imagen;
        $this -> marca = $marca;
        $this -> categoria = $categoria;
        $this -> administrador = $administrador;
        $this -> conexion = new Conexion();
        $this -> articuloDAO = new ArticuloDAO($id, $nombre, $stock, $precio,  $imagen,$marca,$categoria, $administrador);
    }
    
    public function crear(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $administrador = new Administrador($resultado[7]);
        $administrador -> consultar();
        $marca= new Marca($resultado[5]);
        $marca -> consultar();
        $categoria = new Categoria($resultado[6]);
        $categoria -> consultar();
        $this -> nombre = $resultado[1];
        $this -> stock = $resultado[2];
        $this -> precio = $resultado[3];
        $this -> cantidad = $resultado[3];
        $this -> imagen = $resultado[4];
        $this -> administrador = $administrador;
        $this -> marca = $marca;
        $this -> categoria = $categoria;
    }
    
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarTodos($atributo, $direccion, $filas, $pag));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $administrador = new Administrador($resultado[7]);
            $administrador -> consultar();
            $marca = new Marca($resultado[5]);
            $marca -> consultar();
            $categoria = new Categoria($resultado[6]);
            $categoria -> consultar();
            array_push($articulos, new Articulo($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],  $marca, $categoria,$administrador));
                  
        
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    
    public function consultarTodosReporte(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarTodosReporte());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $administrador = new Administrador($resultado[7]);
            $administrador -> consultar();
            $marca = new Marca($resultado[5]);
            $marca -> consultar();
            $categoria = new Categoria($resultado[6]);
            $categoria -> consultar();
            array_push($articulos, new Articulo($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],  $marca, $categoria,$administrador));
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarTotalFilas(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarTotalFilas());
        return $this -> conexion -> extraer()[0];
    }
    
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarFiltro($filtro));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $administrador = new Administrador($resultado[7]);
            $administrador -> consultar();
            $marca = new Marca($resultado[5]);
            $marca -> consultar();
            $categoria = new Categoria($resultado[6]);
            $categoria -> consultar();
            array_push($articulos, new Articulo($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],  $marca, $categoria,$administrador));
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function editarImagen(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> editarImagen());
        $this -> conexion -> cerrar();
    }
    
    public function consultarProductosPorMarca(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarProductosPorMarca());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($articulos, $resultado);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarProductosPorCategoria(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarProductosPorCategoria());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($articulos, $resultado);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
   
    
}




























?>