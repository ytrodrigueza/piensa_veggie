<?php

require_once "persistencia/Conexion.php";
require_once "persistencia/CarritoDAO.php";

class Carrito{
    private $id;
    private $cliente;
    private $conexion;
    private $carritoDAO;
    
/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    public function Carrito($id="", $cliente="" ){
        $this -> id = $id;
        $this -> cliente = $cliente;
        $this -> conexion = new Conexion();
        $this -> carritoDAO = new CarritoDAO($id, $cliente);
    }
   
    public function NumeroProductosCarrito(){
    $this -> conexion -> abrir();
    $this -> conexion -> ejecutar($this -> carritoDAO ->NumeroArticulosCarrito());
    $cantidad = array();
    while(($resultado = $this -> conexion -> extraer()) != null){
        array_push($cantidad, $resultado);
    }
    $this -> conexion -> cerrar();
    return count($cantidad);
}



}
?>