<?php
class ArticuloDAO{
    private $id;
    private $nombre;
    private $stock;
    private $precio;
    private $imagen;
    private $marca;
    private $categoria;
    private $administrador;
    
    public function ArticuloDAO($id="", $nombre="", $stock="", $precio="",  $imagen="",$marca="",$categoria="", $administrador="" ){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> stock = $stock;
        $this -> precio = $precio;
        $this -> imagen = $imagen;
        $this -> marca = $marca;
        $this -> categoria = $categoria;
        $this -> administrador = $administrador;
        
    }
    
    public function crear(){
        return "insert into articulo (nombre,stock, precio,imagen, fk_marca, fk_categoria, fk_administrador)
                values (
                '" . $this -> nombre . "',
                '" . $this -> stock . "',
                '" . $this -> precio . "',
                '" . $this -> imagen . "',
                '" . $this -> marca . "',
                '" . $this -> categoria . "',
                '" . $this -> administrador . "'
                )";
    }
    
    public function consultar(){
        return "select id, nombre,stock, precio, imagen, fk_marca, fk_categoria, fk_administrador
                from articulo
                where id = '" . $this -> id. "'";
    }
    
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        return "select id, nombre,stock, precio, imagen, fk_marca, fk_categoria, fk_administrador
                from articulo " .
                (($atributo != "" && $direccion != "")?"order by " . $atributo . " " . $direccion:"") .
                " limit " . (($pag-1)*$filas) . ", " . $filas;
    }
    
    public function consultarTodosReporte(){
        return "select id, nombre,stock, precio, imagen, fk_marca, fk_categoria, fk_administrador
                from articulo 
                order by nombre";
    }
    
    public function consultarTotalFilas(){
        return "select count(id)
                from articulo";
    }
    
    public function consultarFiltro($filtro){
        return "select id, nombre,stock, precio, imagen, fk_marca, fk_categoria, fk_administrador
                from articulo 
                where nombre like '" . $filtro . "%'";
    }
    
    public function editarImagen(){
        return "update articulo
                set imagen = '" . $this -> imagen . "'
                where id = '" . $this -> id . "'";
    }
    
    public function consultarProductosPorMarca(){
        return "select m.nombre as marca, count(a.id) as cantidad
                from marca m left join articulo a on (m.id = a.fk_marca)
                group by m.nombre
                order by m.nombre asc";
    }
    
    public function consultarProductosPorCategoria(){
        return "select c.nombre, count(a.id) as cantidad
                from categoria c left join articulo a on (c.id= a.fk_categoria)
                group by t.nombre
                order by t.nombre asc";
    }
    
  
   
    
}