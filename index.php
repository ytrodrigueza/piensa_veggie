<?php
require_once "logica/Administrador.php";
require_once "logica/Marca.php";
require_once "logica/Categoria.php";
require_once "logica/Articulo.php";
require_once "logica/Cliente.php";



$pid="";

if(isset($_GET["pid"])){
    
    $pid= base64_decode($_GET["pid"]);
}

?>

<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- JS -->
<script	src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"> </script>
<!-- CSS -->
<link rel="stylesheet" href="css/estilos.css"/>
<!--iconos font awesome-->
<link rel="stylesheet"	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
<!-- letra -->
<link href="https://fonts.googleapis.com/css2?family=Fredericka+the+Great&display=swap" rel="stylesheet"> 
<!--JQUERY libreria de JS--> 
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
<!--JS para usar tooltips separados-->
<script	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script> 
<!-- libreria para hacer gráficos-->
<script src="https://www.gstatic.com/charts/loader.js"></script> 
<!-- letra -->
<link href="https://fonts.googleapis.com/css2?family=Fredericka+the+Great&display=swap" rel="stylesheet">
<!-- para graficos de google static, la libreria loader-->
<script
	src="https://www.gstatic.com/charts/loader.js"></script>

<!-- Titulo pestaña con imágen -->
<title>Piensa Veggie</title>
<link rel="icon" type="image/png" href="img/logo_pesta.png" /> 

</head>

<body>

<?php 

if($pid!=""){
    
    include $pid;
}else{
    
    include "presentacion/inicio.php";
}

?>



	
 <div class="container">
		<div class="row mt-3">
			<div class="row">
				<div class="col text-center text-muted" >
				<i class="fas fa-skull "></i> Y &copy; <?php echo date("Y") ?>	
				</div>			
			</div>
		</div>
		
		
	</div>	
	
	
	
	

</body>
</html>
